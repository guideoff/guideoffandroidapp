package com.group.touristapp.main;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.group.touristapp.R;
import com.group.touristapp.categories.CategoriesActivity;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

public class MainActivity extends AppCompatActivity {
    private MapView map = null;
    private static final GeoPoint TOWN_GEO_POINT = new GeoPoint(49.78498, 22.76728);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_main);

        setupMap();
        createCategoriesButton();
    }

    private void setupMap(){
        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        IMapController mapController = map.getController();
        mapController.setZoom(18.0);
        mapController.setCenter(TOWN_GEO_POINT);
        mapController.animateTo(TOWN_GEO_POINT);
    }

    private void createCategoriesButton(){
        ImageButton imageButton = (ImageButton) findViewById(R.id.categoriesButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slide = new Intent(MainActivity.this, CategoriesActivity.class);
                startActivity(slide);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        map.onPause();
    }
}
