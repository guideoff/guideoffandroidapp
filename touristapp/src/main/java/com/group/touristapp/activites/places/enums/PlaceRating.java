package com.group.touristapp.activites.places.enums;

public enum PlaceRating {
    WORST, GOOD, FINE, FINEST, BEST
}
