package com.group.touristapp.activites.places.enums;

public enum PlaceSearchOptions {
    NEAREST,
    BEST,
    OPEN
}
