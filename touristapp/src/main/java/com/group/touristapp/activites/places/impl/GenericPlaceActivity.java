package com.group.touristapp.activites.places.impl;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.group.touristapp.repositories.PlaceRepository;
import com.group.touristapp.repositories.impl.PlaceRepositoryImpl;


public abstract class GenericPlaceActivity extends AppCompatActivity{
    private PlaceRepository placeRepository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.placeRepository = new PlaceRepositoryImpl();
    }
}
