package com.group.touristapp.activites.places;

import android.media.Image;

import com.group.touristapp.activites.places.enums.PlaceRating;

import org.osmdroid.util.GeoPoint;

public abstract class Place {
    private PlaceRating placeRating;
    private int placeId;
    private String placeName;
    private String placeDesc;
    private GeoPoint placeGeopoint;
    private Image placeImage;

    public Place(PlaceRating placeRating, int placeId, String placeName, String placeDesc, GeoPoint placeGeopoint, Image placeImage){
        this.placeRating = placeRating;
        this.placeId = placeId;
        this.placeName = placeName;
        this.placeDesc = placeDesc;
        this.placeGeopoint = placeGeopoint;
        this.placeImage = placeImage;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceDesc() {
        return placeDesc;
    }

    public void setPlaceDesc(String placeDesc) {
        this.placeDesc = placeDesc;
    }

    public GeoPoint getPlaceGeopoint() {
        return placeGeopoint;
    }

    public void setPlaceGeopoint(GeoPoint placeGeopoint) {
        this.placeGeopoint = placeGeopoint;
    }

    public Image getPlaceImage() {
        return placeImage;
    }

    public void setPlaceImage(Image placeImage) {
        this.placeImage = placeImage;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public PlaceRating getPlaceRating() {
        return placeRating;
    }

    public void setPlaceRating(PlaceRating placeRating) {
        this.placeRating = placeRating;
    }
}
