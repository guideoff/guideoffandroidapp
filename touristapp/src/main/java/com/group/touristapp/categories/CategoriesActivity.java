package com.group.touristapp.categories;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.group.touristapp.R;
import com.group.touristapp.main.MainActivity;

public class CategoriesActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_activity);

        createReturnButton();
    }

    private void createReturnButton(){
        ImageButton imageButton = (ImageButton) findViewById(R.id.returnButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slide = new Intent(CategoriesActivity.this, MainActivity.class);
                startActivity(slide);
            }
        });
    }
}
