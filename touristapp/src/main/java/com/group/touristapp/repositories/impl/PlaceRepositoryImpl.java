package com.group.touristapp.repositories.impl;

import com.group.touristapp.activites.places.Place;
import com.group.touristapp.activites.places.enums.PlaceSearchOptions;
import com.group.touristapp.repositories.PlaceRepository;

import java.util.ArrayList;
import java.util.List;

public class PlaceRepositoryImpl implements PlaceRepository {
    private List<Place> places;

    public PlaceRepositoryImpl(){
        this.places = new ArrayList<>();
    }

    @Override
    public void addPlace(Place place) {
        if(place != null)
            places.add(place);
    }

    @Override
    public void removePlace(Place place) {
        if(place != null)
            places.remove(place);
    }

    @Override
    public Place getPlace(String placeName) {
        for (Place place : places) {
            if(place.getPlaceName().equals(placeName))
                return place;
        }
        return null;
    }

    @Override
    public Place getPlace(int placeId) {
        for (Place place : places){
            if(place.getPlaceId() == placeId)
                return place;
        }
        return null;
    }

    @Override
    public List<Place> getAllPlaces() {
        return places;
    }

    @Override
    public void changeListingOption(PlaceSearchOptions placeSearchOptions) {
        //This one will sort whole list by given search option, I'll implement this one later.
    }
}
