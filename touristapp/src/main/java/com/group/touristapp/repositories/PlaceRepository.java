package com.group.touristapp.repositories;

import com.group.touristapp.activites.places.Place;
import com.group.touristapp.activites.places.enums.PlaceSearchOptions;

import java.util.List;

public interface PlaceRepository {
    void addPlace(Place place);
    void removePlace(Place place);
    Place getPlace(String placeName);
    Place getPlace(int placeId);
    List<Place> getAllPlaces();
    void changeListingOption(PlaceSearchOptions placeSearchOptions);
}
